'use strict';

(function(document) {

    var addButton = document.querySelector('#addBtn')
    var addModale = document.querySelector('#addModale')
    var cancelAddBtn = document.querySelector('#cancelAddBtn')

    function closeModale() {
        addModale.close()
    }

    function openModale() {
        addModale.showModal()
    }
    
    addButton.addEventListener('click', () => {
        openModale()
    })

    cancelAddBtn.addEventListener('click', () => {
        closeModale()
    })

}(document))